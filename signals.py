from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from API.models import User, MyUser


@receiver(post_save, sender=User)
def registration(sender, instance, created, **kwargs):
    if created:
        MyUser.objects.create(user=instance)
