import os
import yaml
from rest_framework.exceptions import ValidationError


def true_or_false(string):
    if isinstance(string, str):
        return bool(yaml.load(string, Loader=yaml.FullLoader))
    else:
        return False


def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.pdf', '.doc', '.docx', '.jpg', '.jpeg', '.png', '.xlsx', '.xls', '.csv']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')

