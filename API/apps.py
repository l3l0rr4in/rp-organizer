from django.apps import AppConfig


class NewAppConfig(AppConfig):
    name = 'API'

    def ready(self):
        import signals