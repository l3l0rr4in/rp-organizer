from django.core.mail import send_mail
from django.utils.html import strip_tags
from django.template.loader import render_to_string
from rest_framework import viewsets, permissions, filters, response, exceptions, generics, status
from rest_framework.decorators import api_view, permission_classes

from permissions import IsAdminOrReadOnly, IsMeOrReadOnly
from API.serializers import *
# Create your views here.


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def send_friend_request(request):
    email = request.data.get('email', False)
    if email:
        # html_message = render_to_string(GODFATHER_REQUEST_BODY, {'context': 'values'}).\
        #             format((request.user.first_name + ' ' + request.user.last_name), request.user.username)
        # message = strip_tags(html_message)
        # send_mail(
        #         subject=GODFATHER_REQUEST,
        #         message=message,
        #         from_email=DEFAULT_FROM_EMAIL,
        #         recipient_list=[email],
        #         html_message=html_message,
        #         fail_silently=False,
        # )
        try:
            user = User.objects.get(username=email)
            friend = MyUser.get_myuser_with_user(user=user)
        except(User.DoesNotExist):
            raise exceptions.NotFound(detail="No matching User")
        else:
            Notification.objects.create(notif_receiver=friend) # Complete with content
        return response.Response({"message": "Email Sended"}, status=status.HTTP_200_OK)
    else:
        raise exceptions.ParseError(detail="Empty 'email' param")


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def accept_friend_request(request):
    email = request.data.get('email', False)
    if email:
        try:
            user = User.objects.get(username=email)
            sender = MyUser.get_myuser_with_user(user=user)
        except(User.DoesNotExist):
            raise exceptions.NotFound(detail="No user use this email")
    else:
        raise exceptions.ParseError(detail="'email' or param required")
    receiver = MyUser.objects.get(user=request.user)
    receiver.myuser_friends.add(sender)
    receiver.save()
    Notification.objects.create(notif_receiver=sender) # Complete with content
    return response.Response({"message": "BFF"}, status=status.HTTP_200_OK)


class MyUserViewSet(viewsets.ModelViewSet):
    queryset = MyUser.objects.all()
    serializer_class = MyUserSerializer
    permission_classes = (permissions.IsAuthenticated, IsMeOrReadOnly)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('user__username', 'user__last_name', 'user__first_name', 'user__email')


class CurrentMyUserView(generics.RetrieveUpdateAPIView):
    serializer_class = MyUserSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        obj = MyUser.get_myuser_with_user(user=self.request.user)
        self.check_object_permissions(self.request, obj)
        return obj

    def perform_update(self, serializer):
        myuser = serializer.save()
        user = myuser.user
        user.username = self.request.data.get('username', user.username)
        user.email = self.request.data.get('email', user.email)
        user.first_name = self.request.data.get('first_name', user.first_name)
        user.last_name = self.request.data.get('last_name', user.last_name)
        user.save()


class PartyViewSet(viewsets.ModelViewSet):
    queryset = Party.objects.all()
    serializer_class = PartySerializer


class PlayerViewSet(viewsets.ModelViewSet):
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer


class PlayerDocumentViewSet(viewsets.ModelViewSet):
    queryset = PlayerDocument.objects.all()
    serializer_class = PlayerDocumentSerializer


class GameViewSet(viewsets.ModelViewSet):
    queryset = Game.objects.all()
    serializer_class = GameSerializer


class GameDocumentViewSet(viewsets.ModelViewSet):
    queryset = GameDocument.objects.all()
    serializer_class = GameDocumentSerializer


class NotificationViewSet(viewsets.ModelViewSet):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer


class ContactViewSet(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
