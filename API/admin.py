from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from API.models import *
# Register your models here.


class MyUserInline(admin.StackedInline):
    model = MyUser
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = (MyUserInline,)
    list_display = ['id', "username", "first_name", "last_name", "email", "is_active", "is_staff"]
    list_filter = ["is_active", "is_staff"]
    search_fields = ["username", "first_name", "last_name", "email"]

    def get_myuser_field(self, obj, param):
        tab = {

        }
        if obj.myuser is not None:
            return tab[param]
        else:
            return ""


class PartyAdmin(admin.ModelAdmin):
    list_display = ["id", "party_name", "party_game"]
    list_filter = ["party_game"]
    search_fields = ["party_name"]


class PlayerAdmin(admin.ModelAdmin):
    list_display = ["id", "player_myuser", "player_party", "player_type"]
    list_filter = ["player_type"]
    search_fields = ["player_myuser__user__username", "player_myuser__user__email",
                     "player_myuser__user__first_name", "player_myuser__user__last_name"]


class PlayerDocumentAdmin(admin.ModelAdmin):
    list_display = ["id", "document_player", "document_type", "document_file"]
    list_filter = ["document_type"]
    search_fields = ["document_player__player_myuser__user__username", "document_player__player_myuser__user__email",
                     "document_player__player_myuser__user__first_name", "document_player__player_myuser__user__last_name"]


class GameAdmin(admin.ModelAdmin):
    list_display = ["id", "game_name"]
    # list_filter = ["player_type"]
    search_fields = ["game_name"]


class GameDocumentAdmin(admin.ModelAdmin):
    list_display = ["id", "document_game", "document_file"]
    list_filter = ["document_game"]
    search_fields = ["document_game__game_name"]


class NotificationAdmin(admin.ModelAdmin):
    list_display = ["id", "notification_title", "notification_receiver", "notification_checked", "created_at",
                    "checked_at"]
    list_filter = ["notification_checked"]
    search_fields = ["notification_title", "notification_subtitle", "notification_body",
                     "notification_receiver__user__username"]


class ContactAdmin(admin.ModelAdmin):
    list_display = ["id", "contact_subject", "contact_names", "contact_email", "contact_phone", "contact_type",
                    "created_at", "contact_admin_handled"]
    list_filter = ["contact_admin_handled", "contact_type"]
    search_fields = ["contact_subject", "contact_names", "contact_email", "contact_phone"]


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Party, PartyAdmin)
admin.site.register(Player, PlayerAdmin)
admin.site.register(PlayerDocument, PlayerDocumentAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(GameDocument, GameDocumentAdmin)
admin.site.register(Notification, NotificationAdmin)
admin.site.register(Contact, ContactAdmin)
