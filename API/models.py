from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from rest_framework.exceptions import NotFound

from utils import validate_file_extension

# Create your models here.


def media_directory_path(instance, filename):
    if isinstance(instance, MyUser):
        return 'users/{0}/avatar/{1}'.format(instance.user.username, filename)
    elif isinstance(instance, GameDocument):
        if instance.document_game is not None:
            name = instance.document_game.game_name
        else:
            return "random/" + filename
        return 'games/{0}/documents/{1}'.format(name, filename)
    elif isinstance(instance, PlayerDocument):
        if instance.document_player is not None and instance.document_player.player_myuser is not None and \
                instance.document_player.player_myuser.user is not None:
            name = instance.document_player.player_myuser.user.username
        else:
            return "random/" + filename
        return 'users/{0}/documents/{1}/'.format(name, filename)


### User ###
class MyUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    myuser_avatar = models.ImageField(upload_to=media_directory_path, null=True)

    myuser_friends = models.ManyToManyField("self", symmetrical=True)

    @staticmethod
    def get_myuser_with_user(user):
        try:
            return MyUser.objects.get(user=user)
        except(MyUser.DoesNotExist):
            raise NotFound(detail='There is no corresponding MyUser')

    def __str__(self):
        if self.user is not None:
            return self.user.username
        else:
            return super(MyUser).__str__()


class Party(models.Model):
    party_name = models.CharField(max_length=2048)
    party_game = models.ForeignKey('Game', on_delete=models.CASCADE, related_name="game_parties", null=True)

    def __str__(self):
        if self.party_name is not None and self.party_game is not None and self.party_game.game_name is not None:
            return self.party_name + " on " + self.party_game.game_name
        else:
            return super(Party).__str__()


class Player(models.Model):
    TYPE_CHOICE = {
        ("PLAYER", "PLAYER"),
        ("DM", "DM")
    }

    player_myuser = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name="myuser_roles", null=True)
    player_party = models.ForeignKey(Party, on_delete=models.CASCADE, related_name="party_players", null=True)
    player_type = models.CharField(max_length=64, choices=TYPE_CHOICE, default="PLAYER")

    def __str__(self):
        if self.player_myuser is not None and self.player_myuser.user is not None and self.player_type is not None and \
                self.player_party is not None and self.player_party.party_name is not None:
            return self.player_myuser.user.username + " " + self.player_type + " " + self.player_party.party_name
        else:
            return super(Player).__str__()


class PlayerDocument(models.Model):
    TYPE_CHOICE = {
        ("CHARACTER", "CHARACTER"),
        ("OTHER", "OTHER")
    }

    document_type = models.CharField(max_length=64, choices=TYPE_CHOICE, default="OTHER")
    document_player = models.ForeignKey(Player, on_delete=models.CASCADE, related_name="player_documents", null=True)
    document_file = models.FileField(upload_to=media_directory_path, validators=[validate_file_extension], null=True)

    def __str__(self):
        if self.document_player is not None and self.document_player.player_myuser is not None and \
                self.document_player.player_myuser.user is not None and self.document_type is not None:
            return self.document_player.player_myuser.user.username + "'s " + self.document_type + " id:" + str(self.pk)
        else:
            return super(PlayerDocument).__str__()


### Game ###
class Game(models.Model):
    game_name = models.CharField(max_length=2048)

    def __str__(self):
        if self.game_name is not None:
            return self.game_name
        else:
            return super(Game).__str__()


class GameDocument(models.Model):
    document_game = models.ForeignKey(Game, on_delete=models.CASCADE, related_name="game_documents", null=True)
    document_file = models.FileField(upload_to=media_directory_path, validators=[validate_file_extension], null=True)

    def __str__(self):
        if self.document_game is not None and self.document_game.game_name is not None:
            return self.document_game.game_name + "'s document id:" + str(self.pk)
        else:
            return super(GameDocument).__str__()


class Notification(models.Model):
    notification_checked = models.BooleanField(default=False)
    notification_title = models.CharField(max_length=1024, null=True)
    notification_subtitle = models.CharField(max_length=1024, null=True)
    notification_body = models.CharField(max_length=4096, null=True)
    notification_extra_data = models.CharField(max_length=4096, null=True)
    notification_receiver = models.ForeignKey(MyUser, on_delete=models.CASCADE, related_name="myuser_notifications",
                                              null=True, blank=True)
    checked_at = models.DateTimeField(default=None, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        if self.notification_title is not None and self.notification_subtitle is not None:
            return self.notification_title + " " + self.notification_subtitle
        else:
            return super(Notification).__str__()


class Contact(models.Model):

    TYPE_CHOICE = (
        ('Technique', 'Technique'),
        ('Commercial', 'Commercial'),
    )

    contact_subject = models.CharField(max_length=256, null=True, blank=True)
    contact_body = models.CharField(max_length=4096, null=True, blank=True)
    contact_admin_handled = models.BooleanField(default=False)
    contact_names = models.CharField(max_length=256, null=True, blank=True)
    contact_email = models.EmailField(null=True, blank=True)
    contact_phone = models.CharField(max_length=18, null=True, blank=True)
    contact_type = models.CharField(max_length=64, choices=TYPE_CHOICE, null=True, blank=True)

    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        if self.contact_subject is not None and self.contact_type is not None:
            return self.contact_subject + " " + self.contact_type
        else:
            return super(Notification).__str__()
