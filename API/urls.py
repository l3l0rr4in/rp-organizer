from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from API.views import *

router = DefaultRouter()
router.register(r'myusers', MyUserViewSet)
router.register(r'parties', PartyViewSet)
router.register(r'players', PlayerViewSet)
router.register(r'players_documents', PlayerDocumentViewSet)
router.register(r'games', GameViewSet)
router.register(r'games_documents', GameDocumentViewSet)
router.register(r'notifications', NotificationViewSet)
router.register(r'contacts', ContactViewSet)

urlpatterns = [
    url(r'send_friend_request', send_friend_request),
    url(r'accept_friend_request', accept_friend_request),
    url(r'^', include(router.urls)),
]